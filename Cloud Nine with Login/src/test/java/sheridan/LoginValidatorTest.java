package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LoginValidatorTest {

	// Test length of user name
	@Test
	public void testIsValidLoginRegular() {
		assertTrue("Invalid login", LoginValidator.isValidLength("LuigiA"));
	}

	@Test
	public void testIsValidLoginException() {
		assertFalse("Invalid login", LoginValidator.isValidLength("Luigi"));
	}

	@Test
	public void testIsValidLoginBoundaryIn() {
		assertTrue("Invalid login", LoginValidator.isValidLength("Luigi1"));
	}

	@Test
	public void testIsValidLoginBoundaryOut() {
		assertTrue("Invalid login", LoginValidator.isValidLength("Luigi6&"));
	}

	// Test is user starts with number or contains a special character
	@Test
	public void testHasNoNumberStartRegular() {
		assertTrue("Invalid login", LoginValidator.hasNoNumberBeggining("LuigiA"));
	}

	@Test
	public void testHasNoNumberStartException() {
		assertFalse("Invalid login", LoginValidator.hasNoNumberBeggining("1Luigi"));
	}

	@Test
	public void testHasNoNumberStartBoundaryIn() {
		assertTrue("Invalid login", LoginValidator.hasNoNumberBeggining("Luigi1"));
	}

	@Test
	public void testHasNoNumberStartBoundaryOut() {
		assertTrue("Invalid login", LoginValidator.hasNoNumberBeggining("Luigi6&"));
	}
}
