package sheridan;

public class LoginValidator {

	private static int MIN_LENGTH = 6;

	public static boolean isValidLoginName(String loginName) {
		boolean length = isValidLength(loginName);
		boolean noNumber = hasNoNumberBeggining(loginName);
		return (length == true && noNumber == true);
	}

	public static boolean isValidLength(String loginName) {
		return (loginName != null && !loginName.contains(" ") && loginName.length() >= MIN_LENGTH);
	}

	public static boolean hasNoNumberBeggining(String loginName) {
		int hasNumber = -1;

		char a = loginName.charAt(0);
		if (Character.isDigit(a)) {
			hasNumber = 1;
		} else {
			hasNumber = 0;
		}

		return (hasNumber == 0);
	}
}
